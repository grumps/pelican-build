Role Name
=========

A simple container provisioner providing an pelican build environment.

Requirements
------------

Per usual a working Python installation is required.

Role Variables
--------------

- `pkgs`: a list of system packages to be installed
- `plugins`: a list of plugins to be installed 

Dependencies
------------

None

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
